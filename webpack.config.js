const path = require('path');

var config = {
   entry: './main.js',
	
   output: {
      path:path.resolve(__dirname, 'dist'),
      filename: 'index.js',
   },
	
   devServer: {
      inline: true,
      port: 5001,
      host: '0.0.0.0',
      public : '34.212.232.119:5001'

   },
	
   module: {
      loaders: [
         {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
				
            query: {
               presets: ['es2015', 'react']
            }
         },
		 {
		  test: /\.html$/,
		  loader: 'html-loader?attrs[]=video:src'
		 }, {
		  test: /\.mp4$/,
		  loader: 'url?limit=10000&mimetype=video/mp4'
		 },
		 { test: /\.css$/, loader: "style-loader!css-loader" }
      ]
	  
   }
}

module.exports = config;
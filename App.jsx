import 'rc-slider/assets/index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { DefaultPlayer as Video } from 'react-html5video';
import Slider from 'rc-slider';


const Range = Slider.Range;

function log(value) {
  console.log(value); //eslint-disable-line
}

class App extends React.Component {
   render() {
      return (
		<div>
			<Content/>
		</div>
      );
   }
}


class Content extends React.Component {
	constructor(props){
		super(props);
		this.state = {
		  'min': 0,
		  'max': 11525,
		  'adStartTime': [0,10,15,26,51,53,117,125,134,145],
		  'qrCodeUrl': 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=id:1_stime:10'
		};
	}
	componentDidMount() {
		
		var player = document.getElementById('player1');
		const adStartTime = this.state.adStartTime;
		var timeout = setTimeout(function(){
		  var interval = setInterval(function(){
			var time = Math.floor(player.currentTime);
		    if(adStartTime.indexOf(time) != -1){
				var str = '{"vdId":1,"sTime":'+time+'}'
				console.log(str);
				const qrCodeUrl = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data="+str;
				this.setState({qrCodeUrl});
		    }
		  }.bind(this),1000);
		}.bind(this),0); 
	  }
   render() {
		var content = {
			 fontSize: '16px'
		}
		var nxtBtn = {
			 fontSize: '14px',
			 padding: '10px 20px'
		}
		var nxtBtnContainer = {
			 textAlign: 'right',
			 padding: '20px 0px'
		}
		var qrCss = {
			position: 'absolute',
			top: '0px',
			height: '65px',
			width: '65px',
			zIndex: '9',
			right: '0px'
		}
		var vidContainer = {
			position: 'relative',
			width: '70%',
    		margin: '10px auto'
		}
      return (
          <div style={content}>
            <h1>
                <img src="./images/Tab.JPG"/>
            </h1>
            <div style={vidContainer}>
			   <img style={qrCss} src={this.state.qrCodeUrl}/>
			   <Video id="player1" autoPlay loop muted
					controls={['PlayPause', 'Seek', 'Time', 'Volume']}
					onCanPlayThrough={() => {
						console.log(this);
					}}>
					<source src="https://storage.googleapis.com/adgencevideos/vidID0.mp4" type="video/mp4" />
				</Video>
			</div>

            <div>
	            <a href="https://34.212.232.119:5002" target="_blank">Post an Ad</a>
	            <span id='video_name'>JAB TAK HAI JAAN - Saans</span>
            </div>
         </div>
      );
   }
}


export default App;